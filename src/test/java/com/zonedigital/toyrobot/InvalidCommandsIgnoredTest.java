package com.zonedigital.toyrobot;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class InvalidCommandsIgnoredTest {
    private final String invalidCommandsTestFile = "InvalidCommandsTestData.txt"  ;

    @Test
    public void testRobotIgnoresInvalidCommands() throws Exception {
        Robot robotUnderTest = new Robot() ;
        List<String> commands = readFile(invalidCommandsTestFile) ;
        commands.forEach(command -> robotUnderTest.doCommand(command.split(" ")));
    }

    private List<String> readFile(String filename) throws IOException {
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        File file = new File(classLoader.getResource(invalidCommandsTestFile).getFile());
        return Files.readAllLines(Paths.get(file.getPath()), Charset.defaultCharset());
    }
}
