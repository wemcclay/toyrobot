package com.zonedigital.toyrobot;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * Unit test for simple RobotSimulator.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({MoveRoundTableTopEdgeTest.class, InvalidCommandsIgnoredTest.class})
public class RobotSimulatorTest
{


}
