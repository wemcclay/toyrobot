package com.zonedigital.toyrobot;

import java.util.Scanner;

/**
 * Toy Robot RobotSimulator !
 *
 */
public class RobotSimulator
{
    public static void main( String[] args )
    {
        Robot robot = new Robot() ;
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Ready ");
            String commandLine = scanner.nextLine();
            // Test for exit command.
            if ("EXIT".equals(commandLine)) {
                System.out.println("Exiting...!");
                break;
            }
            // Get the command string, which will include any parameters, and ask robot
            // to perform operation.
            String[] commandStr = commandLine.split(" ") ;
            robot.doCommand(commandStr) ;
        }
        scanner.close();
    }

}
