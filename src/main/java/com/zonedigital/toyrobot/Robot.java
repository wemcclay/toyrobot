package com.zonedigital.toyrobot;

import java.util.Arrays;
import java.util.List;

/**
 *
 */
public class Robot {

    private final List<String> knownCommands = Arrays.asList("PLACE", "MOVE", "LEFT", "RIGHT", "REPORT") ;
    //
    private final List<String> validDirection = Arrays.asList("NORTH", "SOUTH", "EAST", "WEST") ;
    //
    private static final String PARAMETER_DELIMITER = "," ;
    private static final int ROW_INDEX = 0 ;
    private static final int COLUMN_INDEX = 1 ;

    private static final int COMMAND_STR_INDEX = 0 ;
    private static final int COMMAND_PARAMETER_INDEX = 1 ;

    private static final int NO_PLACE_PARAMETERS = 3 ;

    private int maxRows ;
    private int maxCols ;
    //

    private int BOTTOM ;
    private int TOP ;
    private int LHS ;
    private int RHS ;

    private boolean onTableTop ;
    private boolean atEdge ;

    private int[] location ;
    private String facingDirection ;

    /**
     * Default constructor.
     */
    public Robot() {
        location = new int[2] ;
        maxRows = 5 ;
        maxCols = 5 ;
        BOTTOM = 0 ;
        TOP = maxRows -1 ;
        LHS = 0 ;
        RHS = maxCols - 1 ;
        onTableTop = false;
        atEdge = false ;
    }

    /**
     * Move operation.
     */
    private void move() {
        if ( !atEdge ) {
            switch(facingDirection) {
                case "NORTH":
                    location[COLUMN_INDEX]++ ;
                    break;
                case "SOUTH":
                    location[COLUMN_INDEX]-- ;
                    break;
                case "EAST":
                    location[ROW_INDEX]++ ;
                    break;
                case "WEST":
                    location[ROW_INDEX]-- ;
                    break;
            }
        }
        else {
            switch(facingDirection) {
                case "NORTH":
                    if (location[COLUMN_INDEX] < TOP ) {
                        location[COLUMN_INDEX]++ ;
                    }
                    break;
                case "SOUTH":
                    if ( location[COLUMN_INDEX] > BOTTOM) {
                        location[COLUMN_INDEX]--;
                    }
                    break;
                case "EAST":
                    if ( location[ROW_INDEX] < RHS ) {
                        location[ROW_INDEX]++;
                    }
                    break;
                case "WEST":
                    if ( location[ROW_INDEX] > LHS ) {
                        location[ROW_INDEX]--;
                    }
                    break;
            }
        }
        initialiseOnEdge();

    }

    /**
     * Turn left operation.
     */
    private void turnLeft(){
        switch(facingDirection) {
            case "NORTH":
                facingDirection = "WEST" ;
                break;
            case "SOUTH":
                facingDirection = "EAST" ;
                break;
            case "EAST":
                facingDirection = "NORTH" ;
                break;
            case "WEST":
                facingDirection = "SOUTH" ;
                break;
        }
    }

    /**
     * Turn right operation.
     */
    private void turnRight(){
        switch(facingDirection) {
            case "NORTH":
                facingDirection = "EAST" ;
                break;
            case "SOUTH":
                facingDirection = "WEST" ;
                break;
            case "EAST":
                facingDirection = "SOUTH" ;
                break;
            case "WEST":
                facingDirection = "NORTH" ;
                break;
        }
    }

    /**
     * Report operation.
     */
    private void reportPosition(){
        System.out.println("\t LOCATION : " + location[ROW_INDEX]+ PARAMETER_DELIMITER +location[COLUMN_INDEX]+ PARAMETER_DELIMITER + facingDirection);
    }

    /**
     * Place operation.
     * @param parameters The parameters for the operation. Namely
     *                   the x and y cordinates and the direction to face.
     */
    private void placeOnTableTop(String parameters ) {
        String[] parameterStr = parameters.split(PARAMETER_DELIMITER) ;
        if ( parameterStr.length == NO_PLACE_PARAMETERS ) {
            int x = Integer.valueOf(parameterStr[0]);
            int y = Integer.valueOf(parameterStr[1]);
            String direction = parameterStr[2] ;
            if ( validDirection.contains(direction)
                 && x >= LHS && x <= RHS && y >= BOTTOM && y <= TOP) {
                // can place on table top
                location[ROW_INDEX] = x;
                location[COLUMN_INDEX] = y;
                facingDirection = direction;
                onTableTop = true;
            }

            initialiseOnEdge() ;
        }
    }

    /**
     * Determine if at the edge of the table top.
     */
    private void initialiseOnEdge() {
        if ( location[COLUMN_INDEX] > BOTTOM && location[COLUMN_INDEX] < TOP
                && location[ROW_INDEX] > LHS && location[ROW_INDEX] < RHS ) {
            atEdge = false ;
        }
        else {
            atEdge = true;
        }
    }

    /**
     * Perform the command requested.
     * @param commandStr The command string along with any parameters required by the operation.
     */
    public void doCommand(String[] commandStr) {
        if (knownCommands.contains(commandStr[COMMAND_STR_INDEX])) {
            switch ( commandStr[COMMAND_STR_INDEX] ) {
                case "PLACE":
                    if (commandStr.length > 1) {
                        placeOnTableTop(commandStr[COMMAND_PARAMETER_INDEX]);
                    }
                    break;

                case "MOVE":
                    if (onTableTop) {
                        move();
                    }
                    break;

                case "LEFT":
                    if (onTableTop) {
                        turnLeft();
                    }
                    break;

                case "RIGHT":
                    if (onTableTop ) {
                        turnRight();
                    }
                    break;

                case "REPORT":
                    if(onTableTop) {
                        reportPosition();
                    }
                    break;
            }
        }
    }
}
